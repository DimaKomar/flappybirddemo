//
//  IAP.swift
//  FlappyBirdDemo
//
//  Created by Dima Komar on 12/9/15.
//  Copyright © 2015 dimakomar. All rights reserved.
//

import Foundation
import StoreKit

class IAP: NSObject, SKProductsRequestDelegate {
    
    static let sharedInstance = IAP()
    
     func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        let myProduct = response.products
        for product in myProduct {
            list.append(product)
            print("product added")
            print(product.productIdentifier)
            print(product.localizedTitle)
            
            
        }
        print("list is \(list)")
        
        
        
    }
    
    func canMakePayments() {
        if(SKPaymentQueue.canMakePayments()) {
            print("iap is enabled")
            let productID: NSSet = NSSet(object:"addCoins")
            let request: SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>)
            request.delegate = self
            request.start()
        } else {
            print("please enable IAP")
        }

    }
    
}