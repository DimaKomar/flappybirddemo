//
//  FirstLaunchChecker.swift
//  FlappyBirdDemo
//
//  Created by Dima Komar on 12/28/15.
//  Copyright © 2015 dimakomar. All rights reserved.
//

import Foundation
import StoreKit

class FirstLaunchChecker: NSObject {
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    static let sharedInstance = FirstLaunchChecker()
    
    func checkIfLaunchedFirst() -> Bool {
        
        
        isLaunchedFirst = userDefaults.boolForKey("HasLaunchedSecond")
        
        print("game is launched second = \(isLaunchedFirst)")
        
       
     return isLaunchedFirst
    }
    
}