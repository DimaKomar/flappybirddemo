//
//  GameViewController.swift
//  FlappyBirdDemo
//
//  Created by dimakomar on 11/26/15.
//  Copyright (c) 2015 dimakomar. All rights reserved.
//

import UIKit
import SpriteKit

var heroImage : UIImage!

extension SKNode {
    class func unarchiveFromFile(file : NSString) -> SKNode? {
        if let path = NSBundle.mainBundle().pathForResource(file as String, ofType: "sks") {
            var sceneData = try! NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe)
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as! SKScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
        
    }
}

class GameViewController: UIViewController  {
    var skView: SKView!
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        bar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        bar.shadowImage = UIImage()
        bar.backgroundColor = UIColor(red: 0.0, green: 0.001, blue: 0.001, alpha: 0.001)

        
       
        
        if #available(iOS 8, *) {
        if let scene = GameScene(fileNamed:"GameScene") {
            self.skView = self.view as! SKView
            
            skView.ignoresSiblingOrder = true
            
            scene.scaleMode = .AspectFill
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "quitToLevel:", name: "quitToLevelID", object:nil)
            skView.presentScene(scene)
        }
         } else {
            if let scene = GameScene.unarchiveFromFile("GameScene") as? GameScene {
            
                let skView = self.view as! SKView
                scene.scaleMode = .AspectFill
                skView.ignoresSiblingOrder = true
                skView.showsFPS = false
                NSNotificationCenter.defaultCenter().addObserver(self, selector: "quitToLevel:", name: "quitToLevelID", object:nil)
                skView.presentScene(scene)
            }
        }
    }

    func documentsDirectory() -> String {
        let documentsFolderPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        return documentsFolderPath
    }
    
    func quitToLevel(notification: NSNotification) {
        
        print("loading")
        
        let writePath = (documentsDirectory() as NSString).stringByAppendingPathComponent("Mobile") as NSString

        let getImagePath = writePath.stringByAppendingPathComponent("image1")
        let myImage = UIImage(contentsOfFile: getImagePath)
        
        let controller: RSKImageCropViewController = RSKImageCropViewController(image: myImage!, cropMode: RSKImageCropMode.Circle)
        controller.delegate = self
        controller.rotationEnabled = true
        


        self.navigationController?.pushViewController(controller, animated: true)
    }

   
    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}


extension GameViewController: RSKImageCropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    func imageCropViewControllerDidCancelCrop(controller: RSKImageCropViewController) {
        self.navigationController?.popViewControllerAnimated(true)
    
    }
    
    func imageCropViewController(controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        NSNotificationCenter.defaultCenter().postNotificationName("choseImage", object: croppedImage)
        
        let isLaunchedSec = FirstLaunchChecker.sharedInstance.checkIfLaunchedFirst()
        
        if isLaunchedSec == false {
            
            firstPhoto.removeFromParent()
            firstPhoto1.removeFromParent()
            firstPhoto2.removeFromParent()
            firstPhoto3.removeFromParent()
            firstPhoto4.removeFromParent()
            firstPhoto5.removeFromParent()
            firstPhoto6.removeFromParent()
            firstPhoto7.removeFromParent()
            firstPhoto8.removeFromParent()
            firstPhoto9.removeFromParent()
            firstPhoto10.removeFromParent()
            firstPhoto11.removeFromParent()
            firstPhoto12.removeFromParent()
            firstPhoto13.removeFromParent()
            firstPhoto14.removeFromParent()
            placeHolderPhoto1.removeFromParent()
            placeHolderPhoto2.removeFromParent()
            placeHolderPhoto3.removeFromParent()
            photoIcon.removeFromParent()
            firstBg.removeFromParent()
            
        }

        
        
        
        self.userDefaults.setBool(true, forKey: "HasLaunchedSecond")
        self.userDefaults.synchronize() // don't forget this!!!!
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
}
