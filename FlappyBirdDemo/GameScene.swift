//
//  GameScene.swift
//  FlappyBirdDemo
//
//  Created by dimakomar on 11/26/15.
//  Copyright (c) 2015 dimakomar. All rights reserved.
//

import SpriteKit
import StoreKit
import Social
import AVFoundation
import GameKit

var scorePremium     = 0
var score            = 0
var coinsCount       = 0

var premium          = false
var isAuthentificated   = false


var bestScore = 0
var list = [SKProduct]()

var coinsLabel          : SKLabelNode!

var firstBg: SKSpriteNode!
var firstPhoto: SKSpriteNode!
var firstPhoto1: SKSpriteNode!
var firstPhoto2: SKSpriteNode!
var firstPhoto3: SKSpriteNode!
var firstPhoto4: SKSpriteNode!
var firstPhoto5: SKSpriteNode!
var firstPhoto6: SKSpriteNode!
var firstPhoto7: SKSpriteNode!
var firstPhoto8: SKSpriteNode!
var firstPhoto9: SKSpriteNode!
var firstPhoto10: SKSpriteNode!
var firstPhoto11: SKSpriteNode!
var firstPhoto12: SKSpriteNode!
var firstPhoto13: SKSpriteNode!
var firstPhoto14: SKSpriteNode!
var placeHolderPhoto1: SKSpriteNode!
var placeHolderPhoto2: SKSpriteNode!
var placeHolderPhoto3: SKSpriteNode!
var photoIcon: SKSpriteNode!


class GameScene: SKScene, SKPhysicsContactDelegate, SKPaymentTransactionObserver, GKGameCenterControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //bitmask
    let heroCat:UInt32      = 1 << 0
    let pipeCat:UInt32      = 1 << 1
    let levelCat:UInt32     = 1 << 2
    let scoreCat:UInt32     = 1 << 3
    
    let skyColor = SKColor(colorLiteralRed: 0, green: 191, blue: 255, alpha: 1)
    
    let pipeGap: CGFloat    = 180
    var hero: SKSpriteNode!
    var tapStartLabel: SKLabelNode!
    var more: SKSpriteNode!
    var pipeDown: SKSpriteNode!
    var choseHero: SKSpriteNode!
    var tap1: SKSpriteNode!
    var photoPlace: SKSpriteNode!
    var twitter: SKSpriteNode!
    var facebook: SKSpriteNode!
    var medal: SKSpriteNode!
    var gameCenterButton: SKSpriteNode!
    
    var player:AVAudioPlayer = AVAudioPlayer()
    var player2:AVAudioPlayer = AVAudioPlayer()
    var player3:AVAudioPlayer = AVAudioPlayer()
    
    
    var scrollNode          = SKNode()
    var groundNode          = SKNode()
    var topNode             = SKNode()
    var menuNode            = SKNode()
    var premiumNode         = SKNode()
    
    var playAgainButtonNode = SKNode()

    var pipeUpTex           = SKTexture(imageNamed: "PipeUp")
    var pipeDownTex         = SKTexture(imageNamed: "PipeDown")
    var comletedTexture     = SKTexture(imageNamed: "com")
    var pipesNode           = SKNode()
    var scoreLabel          = SKLabelNode(fontNamed: "AvenirNext-Heavy")
    
    var startLabel          = SKLabelNode(fontNamed: "Avenir")
    
    var isGameOver          = false
    var isStarted           = false
    var photoChanged: Bool = false
    var isTwitterShared: Bool = false
    var isFacebookShared: Bool = false
    
    let groundTex           = SKTexture(imageNamed: "menu")
    let coinTex             = SKTexture(imageNamed: "coin")
    let GCTex             = SKTexture(imageNamed: "GC")
    let heroTexture = SKTexture(imageNamed: "hero")
    var restartString : String!
    var coinsString : String!
    var lifeString : String!
    var zeroCoinsRestartString: String!
    
    var movePipesAndRemove: SKAction!
    var makeSkyRed: SKAction!
    var makeSkyBlue: SKAction!
    var makeGameEnd: SKAction!
    
    var particles: SKEmitterNode!
   
    var p = SKProduct()
    
    var firstLaunch         = true
    
    var movePipes: SKAction!
    let flightSoundURL:NSURL = NSBundle.mainBundle().URLForResource("woosh", withExtension: "mp3")!
    let crashSoundURL:NSURL = NSBundle.mainBundle().URLForResource("Crash2", withExtension: "mp3")!
    let breathSoundURL:NSURL = NSBundle.mainBundle().URLForResource("point", withExtension: "mp3")!
    let backSoundURL:NSURL = NSBundle.mainBundle().URLForResource("Back", withExtension: "mp3")!
    let coinSoundURL:NSURL = NSBundle.mainBundle().URLForResource("coin", withExtension: "mp3")!
    
    var gameCenterAchivmens = [String:GKAchievement]()
    let userDefaults = NSUserDefaults.standardUserDefaults()

    var imageURLMain : NSURL!
    
    //MARK: didMoveToView
    override func didMoveToView(view: SKView) {

       let isLaunchedSec = FirstLaunchChecker.sharedInstance.checkIfLaunchedFirst()
        
        if isLaunchedSec == false {
            
          
            
        }
        
        if isLaunchedFirst == false {
            
            configureFirstLaunchScreen()
            
            coinsCount = coinsCount + 25
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setInteger(coinsCount, forKey: "coins")
            userDefaults.synchronize() // don't forget this!!!!
            
            
        }
        
        
        self.player2 = try! AVAudioPlayer(contentsOfURL: self.breathSoundURL, fileTypeHint: nil)
        self.player2.prepareToPlay()
        
       
        
        
        authenticateLocalPlayer()
        
        if #available(iOS 8, *) {
            // use UIStackView
        
        
        if let particles = SKEmitterNode(fileNamed: "kk.sks") {
            particles.position = CGPoint(x: self.frame.size.width * 0.41, y: self.frame.size.height * -0.05)
            addChild(particles)
        }
        
            
            
            
        } else {
            let sksPath = NSBundle.mainBundle().pathForResource("kk", ofType: "sks")
            let particles: SKEmitterNode = NSKeyedUnarchiver.unarchiveObjectWithFile(sksPath!) as! SKEmitterNode
            particles.position = CGPoint(x: self.frame.size.width * 0.41, y: self.frame.size.height * -0.05)
            addChild(particles)
        }

        if premium {
            score = scorePremium
        } else {
            score = 0
        }
 
        if let imageDataAsDefaults:NSData = self.userDefaults.objectForKey("newTexImage") as? NSData {
            let someImage:UIImage = UIImage(data: imageDataAsDefaults)!
            //check to see if there’s a placeholder namedCameraRollPlaceholder…
            
        let tex:SKTexture = SKTexture(image:someImage)

        let photoFrame = SKSpriteNode(imageNamed: "empty")
        photoFrame.name = "PhotoFrameNode"
        photoFrame.position = position
        
        let pictureNode = SKSpriteNode(texture: tex)
        pictureNode.name = "PictureNode"
        
        let maskNode = SKSpriteNode(imageNamed: "empty2")
        maskNode.name = "Mask"
        
        let cropNode = SKCropNode()
        cropNode.addChild(pictureNode)
        cropNode.maskNode = maskNode
        photoFrame.addChild(cropNode)
        
        self.setupHero(photoFrame)
        self.moveHero()
        
        } else {
        
            heroTexture.filteringMode = .Nearest
            
            self.hero = SKSpriteNode(imageNamed: "hero")
            
            hero.setScale(0.5)
            hero.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
            
            hero.physicsBody = SKPhysicsBody(rectangleOfSize: hero.size)
            hero.physicsBody?.dynamic = true
            hero.physicsBody?.allowsRotation = false
            hero.physicsBody?.categoryBitMask = heroCat
            hero.physicsBody?.collisionBitMask = levelCat | pipeCat
            hero.physicsBody?.contactTestBitMask = levelCat | pipeCat
            
            
            
            self.addChild(hero)

        }
        
        
        coinsCount = userDefaults.integerForKey("coins")
        userDefaults.synchronize()
        
        self.scene!.view!.paused = false
        self.backgroundColor = UIColor.grayColor()
        self.physicsWorld.gravity = CGVectorMake(0.0, -8.0)
        self.physicsWorld.contactDelegate = self
        self.pipeUpTex.filteringMode = .Nearest
        self.pipeDownTex.filteringMode = .Nearest
        
        self.moveHero()
        self.setupGround()
        self.setupSkyLine()
        self.setupInvisibleTop()
        self.hero.physicsBody?.affectedByGravity = false
        // Setup the actions...
        self.makeGameEnd = SKAction.runBlock({
            self.isGameOver = true
        })
        
        
        self.blinkTap()
        
        //setup Gamecenter button
        gameCenterButton = SKSpriteNode(texture: self.GCTex)
        gameCenterButton.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.23)
        gameCenterButton.zPosition = 198
        gameCenterButton.hidden = true
        gameCenterButton.name = "gameCenter"
        self.addChild(gameCenterButton)
        
        let tapTex             = SKTexture(imageNamed: "photo")
        choseHero = SKSpriteNode(texture: tapTex)
        choseHero.position = CGPoint(x: self.frame.size.width * 0.4, y: self.frame.size.height * 0.23)
        choseHero.zPosition = 198
        choseHero.name = "tap"
        choseHero.hidden = true
        self.addChild(choseHero)
        
        let noTex             = SKTexture(imageNamed: "50main")
        tap1 = SKSpriteNode(texture: noTex)
        tap1.position = CGPoint(x: self.frame.size.width * 0.6, y: self.frame.size.height * 0.23)
        tap1.zPosition = 198
        tap1.name = "coinsMain"
        tap1.hidden = true
        self.addChild(tap1)
        
        if isAuthentificated {
            gameCenterButton.hidden = false
            tap1.hidden = false
            choseHero.hidden = false
        }

        
        //setup coins
        coinsLabel              = SKLabelNode(fontNamed: "AvenirNext-Heavy")
        coinsLabel.position     = CGPoint(x: self.frame.size.width * 0.67, y: self.frame.size.height / 1.05)
        coinsLabel.fontColor    = UIColor.whiteColor()
        coinsLabel.text         = String(coinsCount)
        self.addChild(coinsLabel)
        
        //setup coin sprite
        let coin = SKSpriteNode(texture: self.coinTex)
        coin.position = CGPoint(x: self.frame.size.width * 0.62, y: self.frame.size.height * 0.968)
        coin.zPosition = 198
        coin.name = "coin"
        self.addChild(coin)
        
        // setup the score
        scoreLabel.position     = CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height * 0.8)
        scoreLabel.fontColor    = UIColor.whiteColor()
        scoreLabel.fontSize     = 35
        scoreLabel.text         = String(score)
        self.addChild(scoreLabel)
        
       
        
        

    }
    
    
    
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        if scrollNode.speed > 0 {
            if (contact.bodyA.categoryBitMask & scoreCat) ==  scoreCat || (contact.bodyB.categoryBitMask & scoreCat) == scoreCat {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
//                    self.player2 = try! AVAudioPlayer(contentsOfURL: self.breathSoundURL, fileTypeHint: nil)
//                    self.player2.prepareToPlay()
                    self.player2.play()
                })
                score++
                scoreLabel.text = String(score)
                
                switch (score) {
                case 5  : self.incrementCurrentPersentOfAchivments("5points", amount: 5)
                case 10 : self.incrementCurrentPersentOfAchivments("10points", amount: 10)
                case 20 : self.incrementCurrentPersentOfAchivments("20poitns", amount: 20)
                default: break
                }
                
                if (score  % 50 == 0) {
                    self.player3 = try! AVAudioPlayer(contentsOfURL: self.coinSoundURL, fileTypeHint: nil)
                    self.player3.prepareToPlay()
                    self.player3.play()
                    coinsCount = userDefaults.integerForKey("coins")
                    coinsCount = coinsCount + 3
                    userDefaults.setInteger(coinsCount, forKey: "coins")
                    userDefaults.synchronize()
                    coinsLabel.text = String(coinsCount)

                }
                scoreLabel.runAction(SKAction.sequence([
                    SKAction.scaleTo(2.0, duration: NSTimeInterval(0.1)),
                    SKAction.scaleTo(1.0, duration: NSTimeInterval(0.1))
                    ]))
            } else {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    self.player = try! AVAudioPlayer(contentsOfURL: self.crashSoundURL, fileTypeHint: nil)
                    self.player.prepareToPlay()
                    self.player.play()
                })
                
                if score > bestScore {
                    bestScore = score
                    self.saveHightScore("score1", score: bestScore)
                    self.userDefaults.setInteger(bestScore, forKey: "best")
                    self.userDefaults.synchronize() // don't forget this!!!!
                }
                
                scorePremium = score
                self.scrollNode.speed = 0
                if #available(iOS 8, *) {

                    if let particles = SKEmitterNode(fileNamed: "spark.sks") {
                    particles.position = hero.position
                    particles.numParticlesToEmit = 120
                    addChild(particles)
                    }
                
                } else {
                    let sksPath = NSBundle.mainBundle().pathForResource("spark", ofType: "sks")
                    let particles: SKEmitterNode = NSKeyedUnarchiver.unarchiveObjectWithFile(sksPath!) as! SKEmitterNode
                    particles.position = hero.position
                    particles.numParticlesToEmit = 120
                    addChild(particles)

                    
                }
                hero.hidden = true
                let menu = SKSpriteNode(texture: self.groundTex)
                menu.position = CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height * 1.05)
                menu.zPosition = 20
                menu.name = "menu"
                
                
                let silverTex = SKTexture(imageNamed: "silver")
                let bronseTex = SKTexture(imageNamed: "bronze")
                let goldTex = SKTexture(imageNamed: "gold")
                if score == bestScore {
                    self.medal = SKSpriteNode(texture: goldTex)
                } else if score < bestScore && score > 10 {
                    self.medal = SKSpriteNode(texture: silverTex)
                } else {
                    self.medal = SKSpriteNode(texture: bronseTex)
                }
                
                self.medal.zPosition = 10
                self.medal.position = CGPoint(x:  menu.size.height * -0.32, y: menu.size.width * 0.08)
                self.medal.hidden = false
                menu.addChild(self.medal)
                
               
                
                let resultLabel = SKLabelNode(fontNamed: "AvenirNext-Heavy")
                resultLabel.text = "result:\(score)"
                resultLabel.fontSize = 24
                resultLabel.zPosition = 10
                resultLabel.fontColor  = SKColor.whiteColor()
                resultLabel.position = CGPoint(x: menu.size.width * -0.2, y: menu.size.width * -0.14)
                menu.addChild(resultLabel)
                
                bestScore = userDefaults.integerForKey("best")
                let bestScoreLabel = SKLabelNode(fontNamed: "AvenirNext-Heavy")
                bestScoreLabel.text = "best:\(bestScore)"
                bestScoreLabel.fontSize = 24
                bestScoreLabel.zPosition = 10
                bestScoreLabel.fontColor  = SKColor.whiteColor()
                bestScoreLabel.position = CGPoint(x: menu.size.width * -0.2, y: menu.size.width * -0.24)
                menu.addChild(bestScoreLabel)
                
                

               
                
                let restartTexture = SKTexture(imageNamed: "qq")
                let playAgainButton = SKSpriteNode(texture: restartTexture)
                playAgainButton.name = "restart"
                playAgainButton.zPosition = 10
                playAgainButton.position = CGPoint(x:  menu.size.height * 0.4, y: menu.size.width * 0.1)
                playAgainButton.hidden = false
                menu.addChild(playAgainButton)


                let lifeTexture = SKTexture(imageNamed: "play")
                let premiumLife = SKSpriteNode(texture: lifeTexture)
                premiumLife.name = "lifeDisabled"
                premiumLife.zPosition = 10
                premiumLife.position = CGPoint(x: menu.size.height * 0.4, y: menu.size.width * -0.13)
                premiumLife.hidden = false
                menu.addChild(premiumLife)
                
                let restartCoins = SKLabelNode(fontNamed: "AvenirNext-Heavy")
                restartCoins.text = "10 coins extra life"
                restartCoins.fontSize = 12
                restartCoins.zPosition = 10
                restartCoins.fontColor  = SKColor.whiteColor()
                restartCoins.position = CGPoint(x: menu.size.width * 0.22, y: menu.size.width * -0.26)
                menu.addChild(restartCoins)
                
                let moveMenu = SKAction.moveTo(CGPoint(x: self.frame.size.width / 2, y: self.frame.size.height * 0.6), duration: 1)
                menu.runAction(SKAction.sequence([moveMenu, SKAction.waitForDuration(0.1), makeGameEnd]))
                
                delay(1.2) {
                    premiumLife.name = "life"
                }
                
                self.menuNode.addChild(menu)
                
                //self.addChild(premiumNode)
                self.addChild(menuNode)
                
                let moreTexture = SKTexture(imageNamed: "more3")
                more = SKSpriteNode(texture: moreTexture)
                more.position = CGPoint(x: self.frame.size.width * 0.51, y: self.frame.size.height * 0.5)
                more.zPosition = -19
                more.name = "more"
                more.hidden = true
                
                
                
                let moreLabel = SKLabelNode(fontNamed: "AvenirNext-Heavy")
                moreLabel.text = "get more coins"
                moreLabel.fontSize = 20
                moreLabel.zPosition = 21
                moreLabel.fontColor  = SKColor.whiteColor()
                moreLabel.position = CGPoint(x: 0, y: more.frame.origin.y - (more.frame.origin.y * 1.02) )
                more.addChild(moreLabel)
                
                self.addChild(more)
                
                let move50 = SKAction.moveTo(CGPoint(x: self.frame.size.width * 0.4, y: self.frame.size.height * 0.05), duration: 1)
                let moveFacebook = SKAction.moveTo(CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.05), duration: 1)
                let moveTwitter = SKAction.moveTo(CGPoint(x: self.frame.size.width * 0.6, y: self.frame.size.height * 0.05), duration: 1)
                
                
                
                
                
                isTwitterShared = userDefaults.boolForKey("twitter")
                
                if isTwitterShared {
                    self.twitter = SKSpriteNode(texture: comletedTexture)
                    self.twitter.name = "completed"
                    self.twitter.position = CGPoint(x: self.frame.size.width * 0.6, y: self.frame.size.height * -0.05)
                    self.twitter.zPosition = 200
                    
                    self.addChild(self.twitter)
                    twitter.runAction(moveTwitter)
                    


                } else {
                    let twitterTex = SKTexture(imageNamed: "tw")
                    twitter = SKSpriteNode(texture: twitterTex)
                    twitter.position = CGPoint(x: self.frame.size.width * 0.6, y: self.frame.size.height * -0.05)
                    twitter.zPosition = 200
                    twitter.name = "twitter"
                    self.addChild(twitter)
                    twitter.runAction(moveTwitter)

                }
                
                isFacebookShared = userDefaults.boolForKey("facebook")

                if isFacebookShared {
                    self.facebook = SKSpriteNode(texture: comletedTexture)
                    self.facebook.name = "completed"
                    self.facebook.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * -0.05)
                    self.facebook.zPosition = 200
                    
                    self.addChild(self.facebook)
                    facebook.runAction(moveFacebook)
                    
                    
                    
                } else {
                    let facebookTex = SKTexture(imageNamed: "450")
                    facebook = SKSpriteNode(texture: facebookTex)
                    facebook.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * -0.05)
                    facebook.zPosition = 200
                    facebook.name = "facebook"
                    self.addChild(self.facebook)
                    facebook.runAction(moveFacebook)
                }

                
                
                let coinsTexture = SKTexture(imageNamed: "50d")
                let coins = SKSpriteNode(texture: coinsTexture)
                coins.position = CGPoint(x: self.frame.size.width * 0.4, y: self.frame.size.height * -0.05)
                coins.zPosition = 200
                coins.name = "coins"
                self.addChild(coins)
                
                coins.runAction(move50)

            }
        }
    }
    
    //MARK: TOUCH
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        hero.removeActionForKey("moveHeroForever")
        let touch  = touches.first
        let location = touch?.locationInNode(self)
        let node: SKNode = nodeAtPoint(location!)

        if node.name == "coins" {
                                print("coins tapped")
                        for product in list {
                            SVProgressHUD.show()
                            let prodID = product.productIdentifier
                            if prodID == "addCoins" {
                                p = product
                                
                                buyProduct()
                            }
                            
                        }
        }
        
        if node.name == "restart" {
            self.restartString = "restart"
            premium = false
            print("restart tapped")
        }
        
        if node.name == "twitter" {
            let shareTwitter: SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            let vc: UIViewController = self.view!.window!.rootViewController!
            shareTwitter.setInitialText("I've got \(bestScore) points in Tap Photo who can beat me? #TapPhoto https://itunes.apple.com/app/id1073008933")
            shareTwitter.addImage(UIImage(named: "hero5"))
            shareTwitter.completionHandler  = {
                (result:SLComposeViewControllerResult) in
                switch result {
                case SLComposeViewControllerResult.Done : print("done")
                coinsCount = self.userDefaults.integerForKey("coins")
                coinsCount = coinsCount + 25
                self.userDefaults.setInteger(coinsCount, forKey: "coins")
                coinsLabel.text = String(coinsCount)
                
                self.twitter.removeFromParent()
                self.twitter = SKSpriteNode(texture: self.comletedTexture)
                self.twitter.name = "completed"
                self.twitter.position = CGPoint(x: self.frame.size.width * 0.6, y: self.frame.size.height * 0.05)
                self.twitter.zPosition = 200
                self.addChild(self.twitter)
                self.userDefaults.setBool(true, forKey: "twitter")
                self.userDefaults.synchronize() // don't forget this!!!!
                self.player3 = try! AVAudioPlayer(contentsOfURL: self.coinSoundURL, fileTypeHint: nil)
                self.player3.prepareToPlay()
                self.player3.play()
                
                case SLComposeViewControllerResult.Cancelled: print("cancelled")
                }
            }
            vc.presentViewController(shareTwitter, animated: true, completion: nil)
            
        }
        
        if node.name == "facebook" {
            let shareFacebook: SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            let vc: UIViewController = self.view!.window!.rootViewController!
            shareFacebook.setInitialText("I've got \(bestScore) points in Tap Photo who can beat me? #TapPhoto https://itunes.apple.com/app/id1073008933")
            shareFacebook.addImage(UIImage(named: "hero5"))
            shareFacebook.completionHandler  = {
                (result:SLComposeViewControllerResult) in
                switch result {
                case SLComposeViewControllerResult.Done : print("done")
                coinsCount = self.userDefaults.integerForKey("coins")
                coinsCount = coinsCount + 25
                self.userDefaults.setInteger(coinsCount, forKey: "coins")
                coinsLabel.text = String(coinsCount)
                
                self.facebook.removeFromParent()
                self.facebook = SKSpriteNode(texture: self.comletedTexture)

                self.facebook.name = "completed"
                self.facebook.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.05)
                self.facebook.zPosition = 200
                self.addChild(self.facebook)
                self.userDefaults.setBool(true, forKey: "facebook")
                self.userDefaults.synchronize() // don't forget this!!!!
                self.player3 = try! AVAudioPlayer(contentsOfURL: self.coinSoundURL, fileTypeHint: nil)
                self.player3.prepareToPlay()
                self.player3.play()
                case SLComposeViewControllerResult.Cancelled: print("cancelled")
                }
            }
            vc.presentViewController(shareFacebook, animated: true, completion: nil)        }

        
        
        if node.name == "life" {
            self.lifeString = "life"
            coinsCount = userDefaults.integerForKey("coins")
            print(coinsCount)
            if coinsCount > 9 {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                                    self.player = try! AVAudioPlayer(contentsOfURL: self.backSoundURL, fileTypeHint: nil)
                                    self.player.prepareToPlay()
                                    self.player.play()
                                })
                coinsCount = coinsCount - 10
                userDefaults.setInteger(coinsCount, forKey: "coins")
                userDefaults.synchronize() // don't forget this!!!!
                premium = true
                print(coinsCount)
                coinsLabel              = SKLabelNode(fontNamed: "AvenirNext-Heavy")
                coinsLabel.text = String(coinsCount)
                self.zeroCoinsRestartString = "zeroCoinsRestartString"
            } else {
                let moveMoreCoins = SKAction.moveTo(CGPoint(x: self.frame.size.width * 0.51, y: self.frame.size.height * 0.47), duration: 1)
                more.runAction(moveMoreCoins)
                more.hidden = false
            }

        }

        if self.isStarted == false  {
            self.isStarted = true
            
            self.startLabel.removeFromParent()
            self.scene!.view!.paused = false
            
            
        }
        
        if self.scrollNode.speed > 0  {
            
            if node.name == "gameCenter" {
                self.showGameCenter()
                self.moveHero()
            } else if node.name == "tap" {
                self.getPhotoFromSource(UIImagePickerControllerSourceType.PhotoLibrary)
                hero.physicsBody?.affectedByGravity = false
            } else if node.name == "coinsMain" {
                self.moveHero()
                print("coins tapped")
                for product in list {
                    SVProgressHUD.show()
                    let prodID = product.productIdentifier
                    if prodID == "addCoins" {
                        self.p = product

                        self.buyProduct()
                    }
                }
            }
            else {
                counter++
                choseHero.removeFromParent()
                tap1.removeFromParent()
                gameCenterButton.removeFromParent()
                tapStartLabel.removeFromParent()
                
                self.hero.physicsBody?.velocity = CGVectorMake(0, 0)
                self.hero.physicsBody?.applyImpulse(CGVectorMake(0, 150))
                self.hero.physicsBody?.affectedByGravity = true
                self.setupPipes(counter)
 
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.player = try! AVAudioPlayer(contentsOfURL: self.flightSoundURL, fileTypeHint: nil)
                self.player.prepareToPlay()
                self.player.play()
            })
            }
            
        }
        
    }

    override func update(currentTime: CFTimeInterval) {
        
        if let ourHero = self.hero {
            self.hero.zRotation = Utilities.clamp(-1	 , max: 1,
                value: ourHero.physicsBody!.velocity.dy * (ourHero.physicsBody?.velocity.dy < 0 ? 0.003 : 0.001 )
            )
        }
        
            if self.isGameOver {
            self.scene!.view!.paused = false
            
            
            
            if self.restartString != nil {
                restart()
            }
            
            if self.zeroCoinsRestartString != nil {
                restart()
            }
            
            if self.lifeString != nil {
                
            }
        }
    }
    
    func setupHero(hero : SKSpriteNode) -> SKSpriteNode{
        

        heroTexture.filteringMode = .Nearest
        
        self.hero = hero
        
        hero.setScale(0.5)
        hero.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        
        hero.physicsBody = SKPhysicsBody(circleOfRadius: self.hero.size.width / 2)
        hero.physicsBody?.dynamic = true
        hero.physicsBody?.allowsRotation = false
        hero.physicsBody?.categoryBitMask = heroCat
        hero.physicsBody?.collisionBitMask = levelCat | pipeCat
        hero.physicsBody?.contactTestBitMask = levelCat | pipeCat
        
        
        
        self.addChild(hero)
        
        
        return hero
        
    }

    func restart() {
        let scene = GameScene(size: self.size)
        scene.scaleMode = .AspectFill
        self.view?.presentScene(scene)
    }
    var counter = 0

    func blinkTap() {

//        let tpStart             = SKTexture(imageNamed: "tapto")
//        tapStartLabel = SKSpriteNode(texture: tpStart)
        
        tapStartLabel = SKLabelNode(fontNamed: "AvenirNext-Heavy")
        tapStartLabel.text = String("Tap to begin")
        
        tapStartLabel.fontColor    = UIColor.whiteColor()
        tapStartLabel.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.40)
        tapStartLabel.zPosition = 198
        tapStartLabel.name = "tapToBegin"
        tapStartLabel.hidden = false
        
        self.addChild(tapStartLabel)
        
        let blinkLabel = SKAction.fadeAlphaTo(1, duration: 0.1)
        let unblinkLabel = SKAction.fadeAlphaTo(0.2, duration: 0.1)
        let blinkAndUnblink = SKAction.sequence([blinkLabel, unblinkLabel])
        let blinkAndUnblinkForever = SKAction.repeatActionForever(blinkAndUnblink)
        
        tapStartLabel.runAction(blinkAndUnblinkForever, withKey: "blinkAndUnblinkForever")
        
    }
    
    func moveHero() {
        self.hero.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        let moveHeroUp = SKAction.moveTo(CGPoint(x: CGRectGetMidX(self.frame), y: self.frame.height * 0.6), duration: 1)
        let moveHeroDown = SKAction.moveTo(CGPoint(x: CGRectGetMidX(self.frame), y: self.frame.height * 0.5), duration: 1)
        let moveUpAndDown = SKAction.sequence([moveHeroUp, moveHeroDown])
        let moveHeroForever = SKAction.repeatActionForever(moveUpAndDown)
        
        self.hero.runAction(moveHeroForever, withKey: "moveHeroForever")

    }
    
    func setupGround() {
        
        let groundTexSize = groundTex.size()
        let groundTexHeight = groundTexSize.height
        groundTex.filteringMode = .Nearest
        
        self.groundNode.position = CGPointMake(0, self.frame.size.height * -0.2)
        self.groundNode.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(self.frame.size.width, groundTexHeight * 2.0))
        self.groundNode.physicsBody?.dynamic = false
        self.groundNode.physicsBody?.categoryBitMask = levelCat
        
        self.addChild(groundNode)
    }
    
    func setupInvisibleTop() {
        
        let topTexSize = groundTex.size()
        let topTexHeight = topTexSize.height
        
        self.topNode.position = CGPointMake(self.frame.size.width, self.frame.size.height * 1.24)
        self.topNode.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(self.frame.size.width, topTexHeight * 2.0))
        self.topNode.physicsBody?.dynamic = false
        self.topNode.physicsBody?.categoryBitMask = levelCat
        
        self.addChild(topNode)
    }
    
    func setupSkyLine() {
        let skyTex = SKTexture(imageNamed: "sky")
        let skyTexSize = skyTex.size()
        let skyTexWidth = skyTexSize.width
        
        //Skyline actions
        let moveSkySprite = SKAction.moveByX(-skyTexWidth * 2.0, y: 0, duration: NSTimeInterval(0.1 * skyTexWidth * 2.0))
        let resetSkySprite = SKAction.moveByX(skyTexWidth * 2.0, y: 0, duration: 0.0)
        let moveSpritesForever = SKAction.repeatActionForever(SKAction.sequence([moveSkySprite, resetSkySprite]))
        skyTex.filteringMode = .Nearest
        
        for var i:CGFloat = 0; i < 2.0 + self.frame.size.width / (skyTexWidth * 2.0); ++i {
            let sprite  = SKSpriteNode(texture: skyTex)
            sprite.setScale(2.0)
            sprite.zPosition = -300
            sprite.position = CGPointMake(i * sprite.size.width, 300)
            sprite.runAction(moveSpritesForever)
            self.scrollNode.addChild(sprite)
            
        }
        
        self.addChild(scrollNode)
    }
    
    
    func spawnPipes(state : Int) {
        if state == 1 {
        let pipePair        = SKNode()
        pipePair.position   = CGPointMake(self.frame.size.width + self.pipeUpTex.size().width * 2, 0)
        pipePair.zPosition  = -20
        let height          = CGFloat(self.frame.size.height / 4)
        let y               = CGFloat(Double(arc4random())) % height + height * 0.6
        let pipeDown                            = SKSpriteNode(texture: self.pipeDownTex)
        pipeDown.setScale(2.0)
        pipeDown.position                       = CGPointMake(0.0, y + pipeDown.size.height + pipeGap)
        pipeDown.physicsBody                    = SKPhysicsBody(rectangleOfSize: CGSize(width: pipeDown.frame.width - pipeDown.frame.width * 0.1, height: pipeDown.frame.height - pipeDown.frame.height * 0.1 ))
        pipeDown.physicsBody?.dynamic            = false
        pipeDown.physicsBody?.categoryBitMask    = pipeCat
        pipeDown.physicsBody?.contactTestBitMask = heroCat
        pipeDown.zPosition = -100
        pipePair.addChild(pipeDown)
        
        let pipeUp                              = SKSpriteNode(texture: self.pipeUpTex)
        pipeUp.setScale(2.0)
        pipeUp.position                         = CGPointMake(0.0, y)
        pipeUp.physicsBody                      = SKPhysicsBody(rectangleOfSize: CGSize(width: pipeUp.frame.width - pipeUp.frame.width * 0.1, height: pipeUp.frame.height - pipeUp.frame.height * 0.1 ))
        pipeUp.physicsBody?.dynamic              = false
        pipeUp.physicsBody?.categoryBitMask      = pipeCat
        pipeUp.physicsBody?.contactTestBitMask   = heroCat
        pipePair.addChild(pipeUp)

        let contactNode                             = SKNode()
        contactNode.position                        = CGPointMake( pipeDown.size.width + hero.size.width / 2, CGRectGetMidY( self.frame ) )
        contactNode.physicsBody                     = SKPhysicsBody(rectangleOfSize: CGSizeMake( pipeUp.size.width, self.frame.size.height ))
        contactNode.physicsBody?.dynamic             = false
        contactNode.physicsBody?.categoryBitMask     = scoreCat
        contactNode.physicsBody?.contactTestBitMask  = heroCat
        
        pipePair.addChild(contactNode)
        pipePair.runAction(movePipesAndRemove)
        self.pipesNode.addChild(pipePair)
        }
        
    }
    
    func setupPipes(state: Int) {
        if state == 1 {
        let spawn = SKAction.runBlock { () -> Void in
            self.spawnPipes(1)
        }
        
        let delay = SKAction.waitForDuration(NSTimeInterval(1.2))
        let spawnThanDelay = SKAction.sequence([spawn, delay])
        let spawnThanDelayForever = SKAction.repeatActionForever(spawnThanDelay)
        
        let distanceToMove = CGFloat(self.frame.size.width + 2.0 * self.pipeUpTex.size().width)
        self.movePipes = SKAction.moveByX(-distanceToMove, y: 0.0, duration: NSTimeInterval(0.005 * distanceToMove ))
        let removePipes = SKAction.removeFromParent()
        movePipesAndRemove = SKAction.sequence([movePipes, removePipes])
        self.runAction(spawnThanDelayForever)
        
        self.addChild(self.pipesNode)
        }
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    //MARK: GC stack
    
    func authenticateLocalPlayer(){
        let localPlayer: GKLocalPlayer = GKLocalPlayer.localPlayer()
        localPlayer.authenticateHandler = { (viewController, error) -> Void in
            if viewController != nil {
                let vc = self.view!.window!.rootViewController!
                vc.presentViewController(viewController!, animated: true, completion: nil)
               
            } else {
                print("Authenticated as \(localPlayer.authenticated)")
                self.gameCenterAchivmens.removeAll()
                self.loadAchivmentPercentages()
            }
            let fadeIn = SKAction.fadeInWithDuration(1)
            
            
            
            
            self.gameCenterButton.alpha = 0.0
            self.gameCenterButton.hidden = false
            self.gameCenterButton.runAction(fadeIn)
            
            self.tap1.alpha = 0.0
            self.tap1.hidden = false
            self.tap1.runAction(fadeIn)
            
            self.choseHero.alpha = 0.0
            self.choseHero.hidden = false
            self.choseHero.runAction(fadeIn)
            
            isAuthentificated = true
        }
        
    }
    
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController) {
        
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func showGameCenter() {
        let gameCenterController = GKGameCenterViewController()
        gameCenterController.gameCenterDelegate = self
        let vc = self.view!.window!.rootViewController!
        vc.presentViewController(gameCenterController, animated: true, completion: nil)
    }
    
    func saveHightScore(identyfier: String, score: Int) {
        if GKLocalPlayer.localPlayer().authenticated {
            let scoreReporter = GKScore(leaderboardIdentifier: identyfier)
            scoreReporter.value = Int64(score)
            let scoreArray: [GKScore] = [scoreReporter]
            
            GKScore.reportScores(scoreArray, withCompletionHandler: { error -> Void in
                if error == nil {
                    print("score posted")
                } else {
                    print("error = \(error)")
                }
            })
        }
    }
    
    func loadAchivmentPercentages() {
        print("getting percentage of past achivments")
        GKAchievement.loadAchievementsWithCompletionHandler( { (allAchivments, error) -> Void in
            
            if error != nil {
                print("error != nil \(error)")
            } else {
                
                if allAchivments != nil {
                    
                    for theAchivment in allAchivments! {
                    print("ach name = \(theAchivment)")
                        if let singleAchivment: GKAchievement = theAchivment {
                            self.gameCenterAchivmens[singleAchivment.identifier!] = singleAchivment
                        }
                    }
                    
                    for (id, achivment) in self.gameCenterAchivmens {
                        print("id is = \(id), ach is = \(achivment)")
                    }
                    
                }
            }
            
        })
    }
    
    func incrementCurrentPersentOfAchivments(identifier: String, amount: Double ) {
        if GKLocalPlayer.localPlayer().authenticated {
            var currentPercentFound: Bool  = false
            if gameCenterAchivmens.count != 0 {
                
                for (id, achivment) in self.gameCenterAchivmens {
                    
                    if id == identifier {
                        currentPercentFound = true
                        var currentPercent = achivment.percentComplete
                        currentPercent = currentPercent + amount
                        reportAchivment(identifier, percentComplete: currentPercent)
                        
                        break
                    }
                }
            }
            if currentPercentFound == false {
                reportAchivment(identifier, percentComplete: amount)
            }
        }
    }
    
    func reportAchivment(identifier: String, percentComplete: Double) {
        let achivment = GKAchievement(identifier: identifier)
        achivment.percentComplete = percentComplete
        let achivmentArray = [achivment]
        
        GKAchievement.reportAchievements(achivmentArray, withCompletionHandler: ( { error -> Void in
        
            if error != nil {
                print("error")
            } else {
                print("report the achivment with the persent complete \(percentComplete)")
                self.gameCenterAchivmens.removeAll()
                self.loadAchivmentPercentages()
            }
            
        }))
        
    }

    //MARK: In-App stack
    
    func buyProduct() {
        print("buy +" + p.productIdentifier)
        let payment = SKMutablePayment(product: p)
        
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        SKPaymentQueue.defaultQueue().addPayment(payment)
        
    }
    
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        print(transactions.count)
        for transaction in transactions {
            print(transaction.error)
            switch transaction.transactionState {
            case .Purchased:
                queue.finishTransaction(transaction)
                coinsCount = coinsCount + 50
                userDefaults.setInteger(coinsCount, forKey: "coins")
                userDefaults.synchronize() // don't forget this!!!!
                coinsLabel.text = String(coinsCount)
                self.player3 = try! AVAudioPlayer(contentsOfURL: self.coinSoundURL, fileTypeHint: nil)
                self.player3.prepareToPlay()
                self.player3.play()
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                SKPaymentQueue.defaultQueue().removeTransactionObserver(self)
                self.moveHero()
                SVProgressHUD.dismiss()
                break;
            case .Failed:
                self.moveHero()
                SVProgressHUD.dismiss()
                
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                break;
            default:
                break
            }
        }
    }
    //MARK: Image picker stack
    
    func getPhotoFromSource(source:UIImagePickerControllerSourceType ){
        if UIImagePickerController.isSourceTypeAvailable(source)
            
        {
            let imagePicker = UIImagePickerController()
            
            imagePicker.modalPresentationStyle = .CurrentContext
            
            imagePicker.delegate = self
            
            imagePicker.sourceType = source
            
            imagePicker.allowsEditing = false
            if (source == .Camera){
                
                imagePicker.cameraDevice = .Front
                
            }
            let vc:UIViewController = self.view!.window!.rootViewController!
            
            vc.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
   
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        print("object dissmissed")
        picker.delegate = nil
        self.moveHero()
    }

    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "choseImage:", name: "choseImage", object:nil)
        
        SVProgressHUD.show()
        dispatch_async(dispatch_get_main_queue(),{
            
            
       
        
        if (picker.sourceType == UIImagePickerControllerSourceType.PhotoLibrary || picker.sourceType == UIImagePickerControllerSourceType.Camera ) {
            //do something with the image we picked
            if let cameraRollPicture = info[UIImagePickerControllerOriginalImage] as? UIImage {

                let save = self.saveImage(cameraRollPicture, path: self.fileInDocumentsDirectory("image1"))
                print("saving is \(save)")
                NSNotificationCenter.defaultCenter().postNotificationName("quitToLevelID", object: nil)
            }
        }
    
        picker.dismissViewControllerAnimated(true, completion: nil)
        self.moveHero()
        picker.delegate = nil
        SVProgressHUD.dismiss()
        
        })
        
    }

    func choseImage(notification: NSNotification) {
        guard let url = notification.object else {
            return
        }
        let blob = url as! UIImage  // or as! Sting or as! Int
        
//        let tex:SKTexture = SKTexture(image:blob)
        let photoFrame = SKSpriteNode(imageNamed: "empty")
        photoFrame.name = "PhotoFrameNode"
        photoFrame.position = position
        
        let maskNode = SKSpriteNode(imageNamed: "empty2")
        maskNode.name = "Mask"
        
        let maskImage = UIImage(named: "empty2")
        
        let newTexImage = self.resizeImage(blob, newWidth: (maskImage?.size.width)!)
        let newTex:SKTexture = SKTexture(image:newTexImage)
        
        userDefaults.setObject( UIImagePNGRepresentation(newTexImage), forKey:"newTexImage")
        userDefaults.synchronize()
        
        let pictureNode = SKSpriteNode(texture: newTex)
        pictureNode.name = "PictureNode"
        
        let cropNode = SKCropNode()
        cropNode.addChild(pictureNode)
        cropNode.maskNode = maskNode
        photoFrame.addChild(cropNode)
        self.hero.removeFromParent()
        
        if firstPhoto != nil {
            firstPhoto.removeFromParent()
        }
        
        self.setupHero(photoFrame)
        self.hero.physicsBody?.affectedByGravity = false
        self.moveHero()

    }

    func saveImage (image: UIImage, path: String ) -> Bool{
        
        let pngImageData = UIImagePNGRepresentation(image)
        
//        let jpgImageData = UIImageJPEGRepresentation(image, 0)   // if you want to save as JPEG
        let result = pngImageData!.writeToFile(path, atomically: true)
        print("saving")
        return result
        
    }
    
    func documentsDirectory() -> String {
        let documentsFolderPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0]
        return documentsFolderPath
    }
    
    func fileInDocumentsDirectory(filename: String) -> String {
        
        let writePath = (documentsDirectory() as NSString).stringByAppendingPathComponent("Mobile")
        
        if (!NSFileManager.defaultManager().fileExistsAtPath(writePath)) {
            do {
                try NSFileManager.defaultManager().createDirectoryAtPath(writePath, withIntermediateDirectories: false, attributes: nil) }
            catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        return (writePath as NSString).stringByAppendingPathComponent(filename)
    }

    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    //MARK: first screen stack
    
    func configureFirstLaunchScreen() {
        
        let firstBgTex  = SKTexture(imageNamed: "24")
        firstBg = SKSpriteNode(texture: firstBgTex)
        firstBg.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.04)
        firstBg.zPosition = 199
        firstBg.name = "tap"
        firstBg.hidden = false

        let yellowTex  = SKTexture(imageNamed: "yellowLine")
        firstPhoto = SKSpriteNode(color: UIColor(hexString: "57900b"), size: yellowTex.size())
        firstPhoto.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.0402)
        firstPhoto.zPosition = 200
        firstPhoto.name = "tap"
        firstPhoto.hidden = false
        
        firstPhoto1 = SKSpriteNode(color: UIColor(hexString: "6aa71c"), size: yellowTex.size())
        firstPhoto1.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.12)
        firstPhoto1.zPosition = 200
        firstPhoto1.name = "tap"
        firstPhoto1.hidden = false
        
        firstPhoto2 = SKSpriteNode(color: UIColor(hexString: "95b90d"), size: yellowTex.size())
        firstPhoto2.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.196)
        firstPhoto2.zPosition = 200
        firstPhoto2.name = "tap"
        firstPhoto2.hidden = false
        
        firstPhoto3 = SKSpriteNode(color: UIColor(hexString: "b7ca0e"), size: yellowTex.size())
        firstPhoto3.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.272)
        firstPhoto3.zPosition = 200
        firstPhoto3.name = "tap"
        firstPhoto3.hidden = false
        
        firstPhoto4 = SKSpriteNode(color: UIColor(hexString: "d8d605"), size: yellowTex.size())
        firstPhoto4.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.348)
        firstPhoto4.zPosition = 200
        firstPhoto4.name = "tap"
        firstPhoto4.hidden = false
        
        firstPhoto5 = SKSpriteNode(color: UIColor(hexString: "e8d904"), size: yellowTex.size())
        firstPhoto5.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.424)
        firstPhoto5.zPosition = 200
        firstPhoto5.name = "tap"
        firstPhoto5.hidden = false
        
        firstPhoto6 = SKSpriteNode(color: UIColor(hexString: "fbd601"), size: yellowTex.size())
        firstPhoto6.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.5)
        firstPhoto6.zPosition = 200
        firstPhoto6.name = "tap"
        firstPhoto6.hidden = false
        
        firstPhoto7 = SKSpriteNode(color: UIColor(hexString: "fbc002"), size: yellowTex.size())
        firstPhoto7.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.576)
        firstPhoto7.zPosition = 200
        firstPhoto7.name = "tap"
        firstPhoto7.hidden = false
        
        firstPhoto8 = SKSpriteNode(color: UIColor(hexString: "fba701"), size: yellowTex.size())
        firstPhoto8.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.652)
        firstPhoto8.zPosition = 200
        firstPhoto8.name = "tap"
        firstPhoto8.hidden = false

        firstPhoto9 = SKSpriteNode(color: UIColor(hexString: "fe8b05"), size: yellowTex.size())
        firstPhoto9.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.728)
        firstPhoto9.zPosition = 200
        firstPhoto9.name = "tap"
        firstPhoto9.hidden = false
        
        firstPhoto10 = SKSpriteNode(color: UIColor(hexString: "fb6b03"), size: yellowTex.size())
        firstPhoto10.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.804)
        firstPhoto10.zPosition = 200
        firstPhoto10.name = "tap"
        firstPhoto10.hidden = false
        
        firstPhoto11 = SKSpriteNode(color: UIColor(hexString: "ec5403"), size: yellowTex.size())
        firstPhoto11.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.88)
        firstPhoto11.zPosition = 200
        firstPhoto11.name = "tap"
        firstPhoto11.hidden = false
        
        firstPhoto12 = SKSpriteNode(color: UIColor(hexString: "d24700"), size: yellowTex.size())
        firstPhoto12.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.956)
        firstPhoto12.zPosition = 200
        firstPhoto12.name = "tap"
        firstPhoto12.hidden = false

        firstPhoto13 = SKSpriteNode(color: UIColor(hexString: "fff"), size: yellowTex.size())
        firstPhoto13.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 1.032)
        firstPhoto13.zPosition = 200
        firstPhoto13.name = "tap"
        firstPhoto13.hidden = false

        firstPhoto14 = SKSpriteNode(color: UIColor(hexString: "d24700"), size: yellowTex.size())
        firstPhoto14.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 1.03)
        firstPhoto14.zPosition = 200
        firstPhoto14.name = "tap"
        firstPhoto14.hidden = false

        let ph1    = SKTexture(imageNamed: "ph1")
        let ph2    = SKTexture(imageNamed: "ph2")
        let ph3    = SKTexture(imageNamed: "ph3")
        let photoIconTex    = SKTexture(imageNamed: "ic")
        
        placeHolderPhoto1 = SKSpriteNode(texture: ph1)
        placeHolderPhoto1.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * -0.2)
        placeHolderPhoto1.zPosition = 201
        placeHolderPhoto1.name = "tap"
        placeHolderPhoto1.hidden = false
        
        placeHolderPhoto2 = SKSpriteNode(texture: ph2)
        placeHolderPhoto2.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * -0.2)
        placeHolderPhoto2.zPosition = 202
        placeHolderPhoto2.name = "tap"
        placeHolderPhoto2.hidden = false
        
        placeHolderPhoto3 = SKSpriteNode(texture: ph3)
        placeHolderPhoto3.position = CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * -0.2)
        placeHolderPhoto3.zPosition = 203
        placeHolderPhoto3.name = "tap"
        placeHolderPhoto3.hidden = false
        
        photoIcon = SKSpriteNode(texture: photoIconTex)
        photoIcon.position = CGPoint(x: self.frame.size.width * -0.2, y: self.frame.size.height * -0.5)
        photoIcon.zPosition = 203
        photoIcon.name = "tap"
        photoIcon.hidden = false
        
        
        
       
        let scale = SKAction.scaleXTo(self.frame.size.width, duration: 1.4)
        let alpha = SKAction.fadeAlphaTo(1, duration: 0.5)
        
        let move1 = SKAction.moveTo(CGPoint(x: self.frame.size.width * 0.4, y: self.frame.size.height * 0.73), duration: 5, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 0)
        let move2 = SKAction.moveTo(CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.75), duration: 5, delay: 1.5, usingSpringWithDamping: 1, initialSpringVelocity: 0)
        let move3 = SKAction.moveTo(CGPoint(x: self.frame.size.width * 0.6, y: self.frame.size.height * 0.73), duration: 5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 0)
        let move4 = SKAction.moveTo(CGPoint(x: self.frame.size.width * 0.5, y: self.frame.size.height * 0.4), duration: 25, delay: 2, usingSpringWithDamping: 0.7, initialSpringVelocity: 0)
        let move5 = SKAction.moveTo(CGPoint(x: self.frame.size.width * 0.6, y: self.frame.size.height * 0.3), duration: 25, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0)
        let move6 = SKAction.moveTo(CGPoint(x: self.frame.size.width * 0.46, y: self.frame.size.height * 0.4), duration: 25, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0)
        let move7 = SKAction.moveTo(CGPoint(x: self.frame.size.width * 0.55, y: self.frame.size.height * 0.3), duration: 25, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0)
        let move8 = SKAction.moveTo(CGPoint(x: self.frame.size.width * -0.1, y: self.frame.size.height * 0.01), duration: 25, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0)
        
        let allMoves = SKAction.sequence([move4,move5,move6,move7,move8])
        let repeatedMoves = SKAction.repeatActionForever(allMoves)
        
        let rotate1 = SKAction.rotateByAngle(0.3, duration: 5)
        let rotate2 = SKAction.rotateByAngle(0.0, duration: 5)
        let rotate3 = SKAction.rotateByAngle(-0.3, duration: 5)
        
        placeHolderPhoto1.runAction(SKAction.group([move1,rotate1]))
        placeHolderPhoto2.runAction(SKAction.group([move2,rotate2]))
        placeHolderPhoto3.runAction(SKAction.group([move3,rotate3]))
        
        photoIcon.runAction(repeatedMoves)
        
        
        firstPhoto.alpha = 0.0
        firstPhoto.runAction(SKAction.sequence([SKAction.waitForDuration(1.0), SKAction.group([scale,alpha])]))
        
        firstPhoto1.alpha = 0.0
        firstPhoto1.runAction(SKAction.sequence([SKAction.waitForDuration(1.1), SKAction.group([scale,alpha])]))
        
        firstPhoto2.alpha = 0.0
        firstPhoto2.runAction(SKAction.sequence([SKAction.waitForDuration(1.2), SKAction.group([scale,alpha])]))
        
        firstPhoto3.alpha = 0.0
        firstPhoto3.runAction(SKAction.sequence([SKAction.waitForDuration(1.3), SKAction.group([scale,alpha])]))
        
        firstPhoto4.alpha = 0.0
        firstPhoto4.runAction(SKAction.sequence([SKAction.waitForDuration(1.4), SKAction.group([scale,alpha])]))
        
        firstPhoto5.alpha = 0.0
        firstPhoto5.runAction(SKAction.sequence([SKAction.waitForDuration(1.5), SKAction.group([scale,alpha])]))
        
        firstPhoto6.alpha = 0.0
        firstPhoto6.runAction(SKAction.sequence([SKAction.waitForDuration(1.6), SKAction.group([scale,alpha])]))
        
        firstPhoto7.alpha = 0.0
        firstPhoto7.runAction(SKAction.sequence([SKAction.waitForDuration(1.7), SKAction.group([scale,alpha])]))
        
        firstPhoto8.alpha = 0.0
        firstPhoto8.runAction(SKAction.sequence([SKAction.waitForDuration(1.8), SKAction.group([scale,alpha])]))
        
        firstPhoto9.alpha = 0.0
        firstPhoto9.runAction(SKAction.sequence([SKAction.waitForDuration(1.9), SKAction.group([scale,alpha])]))
        
        firstPhoto10.alpha = 0.0
        firstPhoto10.runAction(SKAction.sequence([SKAction.waitForDuration(2.0), SKAction.group([scale,alpha])]))
        
        firstPhoto11.alpha = 0.0
        firstPhoto11.runAction(SKAction.sequence([SKAction.waitForDuration(2.1), SKAction.group([scale,alpha])]))
        
        firstPhoto12.alpha = 0.0
        firstPhoto12.runAction(SKAction.sequence([SKAction.waitForDuration(2.2), SKAction.group([scale,alpha])]))
        
        firstPhoto13.alpha = 0.0
        firstPhoto13.runAction(SKAction.sequence([SKAction.waitForDuration(2.3), SKAction.group([scale,alpha])]))
        
        firstPhoto14.alpha = 0.0
        firstPhoto14.runAction(SKAction.sequence([SKAction.waitForDuration(2.4), SKAction.group([scale,alpha])]))
        
        self.addChild(firstBg)
        self.addChild(photoIcon)
        self.addChild(placeHolderPhoto1)
        self.addChild(placeHolderPhoto2)
        self.addChild(placeHolderPhoto3)
        self.addChild(firstPhoto)
        self.addChild(firstPhoto1)
        self.addChild(firstPhoto2)
        self.addChild(firstPhoto3)
        self.addChild(firstPhoto4)
        self.addChild(firstPhoto5)
        self.addChild(firstPhoto6)
        self.addChild(firstPhoto7)
        self.addChild(firstPhoto8)
        self.addChild(firstPhoto9)
        self.addChild(firstPhoto10)
        self.addChild(firstPhoto11)
        self.addChild(firstPhoto12)
        self.addChild(firstPhoto13)
        self.addChild(firstPhoto14)
    }

}

