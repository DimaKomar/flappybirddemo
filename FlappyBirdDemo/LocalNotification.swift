//
//  LocalNotification.swift
//  FlappyBirdDemo
//
//  Created by Dima Komar on 12/14/15.
//  Copyright © 2015 dimakomar. All rights reserved.
//

import Foundation

@available(iOS 8.0, *)
class LocalNotification {
    
    static let sharedInstance = LocalNotification()
    
    func setUpNotifications() {
        let firstAction: UIMutableUserNotificationAction = UIMutableUserNotificationAction()
        firstAction.identifier = "coins_action"
        firstAction.title = "coins action"
        
        firstAction.activationMode = UIUserNotificationActivationMode.Foreground
        firstAction.destructive = false
        firstAction.authenticationRequired = false
        
        
        //category
        let firstCateory = UIMutableUserNotificationCategory()
        firstCateory.identifier = "first_category"
        
        let defaultActions = [firstAction]
        
        firstCateory.setActions(defaultActions, forContext: UIUserNotificationActionContext.Default)
        
        //NSSet of our categries
        
        let categories = NSSet(objects: firstCateory)
        
        
        let mySettings: UIUserNotificationSettings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: categories as? Set<UIUserNotificationCategory>)
        UIApplication.sharedApplication().registerUserNotificationSettings(mySettings)
    }
    
    
}