//
//  AppDelegate.swift
//  FlappyBirdDemo
//
//  Created by dimakomar on 11/26/15.
//  Copyright © 2015 dimakomar. All rights reserved.
//

import UIKit
import StoreKit
import AVFoundation

var isMounthReward      = false
var isNewMounth         = true
var isLaunchedFirst     = true

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    let date = NSDate(timeIntervalSinceNow: 1)
    let calendar = NSCalendar.currentCalendar()

    var startDate = NSDate()

    
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        
       //FirstLaunchChecker.sharedInstance.checkIfLaunchedFirst()
       IAP.sharedInstance.canMakePayments()

        let components = calendar.components([.Day] , fromDate: date)
        let week = components.day
        
        var startOfTheWeek : NSDate?
        var endOfWeek : NSDate?
        var weekDuration : NSTimeInterval = 0
        calendar.rangeOfUnit(NSCalendarUnit.WeekOfYear, startDate: &startOfTheWeek, interval: &weekDuration, forDate: NSDate())
        startOfTheWeek = startOfTheWeek?.dateByAddingTimeInterval(weekDuration) // start of the next week
        calendar.rangeOfUnit(NSCalendarUnit.WeekOfYear, startDate: &startOfTheWeek, interval: &weekDuration, forDate: startOfTheWeek ?? NSDate())
        endOfWeek = startOfTheWeek?.dateByAddingTimeInterval(weekDuration - 1) // end of the next week
        
        let componentsEnd = calendar.components([.Day] , fromDate: endOfWeek!)
        let endDay = componentsEnd.day

        
        print(week)
        print(endDay)

        
        
        
        if #available(iOS 8.0, *) {
            LocalNotification.sharedInstance.setUpNotifications()
            checkIfWeekChanged()
        }
        
        
        return true
    }
 
    
    
    func application(application: UIApplication, performFetchWithCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
         
            completionHandler(UIBackgroundFetchResult.NoData)

        fireLocalPushNitification()
        
    }
    
    func checkIfWeekChanged() {
        
        let components = calendar.components([.WeekOfMonth] , fromDate: date)
        let week = components.weekOfMonth
        let posiblePastweek = userDefaults.integerForKey("week")
        print(week)
        if week != posiblePastweek {
            UIApplication.sharedApplication().setMinimumBackgroundFetchInterval(NSTimeInterval(9999))
        }
        
        let week2 = components.weekOfMonth
        userDefaults.setInteger(week2, forKey: "week")
        userDefaults.synchronize()
        
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }

     func fireLocalPushNitification() {
        if #available(iOS 8.0, *) {
            let localNotification:UILocalNotification = UILocalNotification()
            localNotification.category = "first_category"
            localNotification.alertAction = "get coins"
            localNotification.alertBody = "Hey you got more coins, to play with extra lifes!"
            localNotification.fireDate = nil
            localNotification.soundName = UILocalNotificationDefaultSoundName
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "addCoins:", name: "actionOnePressed", object: nil) //for no action
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "addCoins:", name: "actionOnePressed1", object: nil) // for swipe or pull action
            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        }
    }
    
    func addCoins(notification: NSNotification) {
        coinsCount = userDefaults.integerForKey("coins")
        coinsCount = coinsCount + 5
        userDefaults.setInteger(coinsCount, forKey: "coins")
        userDefaults.synchronize()
        print("added coint ")
        coinsLabel.text = String(coinsCount)
    }
    
    @available(iOS 8.0, *)
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, completionHandler: () -> Void) {
        if identifier == "coins_action" {
            NSNotificationCenter.defaultCenter().postNotificationName("actionOnePressed", object: nil)
        }
        completionHandler()
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        NSNotificationCenter.defaultCenter().postNotificationName("actionOnePressed1", object: nil)
    }

}
